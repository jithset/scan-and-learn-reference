# quarkus-keycloak-rest Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Links

```shell
# Swagger
http://localhost:8085/q/swagger-ui/
```
