package io.github.jithset.keycloak_admin.dto;

import lombok.Getter;
import lombok.Setter;

public class UsersCreateDTO {
    @Getter @Setter
    private String userId;
}
