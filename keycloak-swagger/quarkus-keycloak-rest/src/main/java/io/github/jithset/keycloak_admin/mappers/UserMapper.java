package io.github.jithset.keycloak_admin.mappers;

import io.github.jithset.keycloak_admin.dto.UsersDTO;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.List;

public class UserMapper {

//    public static UsersDTO fromRepresentationToDto(UserRepresentation userRepresentation) {
//
//    }

    public static UserRepresentation fromDtoToRepresentation(UsersDTO usersDTO) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(usersDTO.getUsername());
        userRepresentation.setEmail(usersDTO.getEmail());
        userRepresentation.setEnabled(true);
        List<CredentialRepresentation> credentialRepresentationList = new ArrayList<>();
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(usersDTO.getPassword());
        credential.setTemporary(false);
        credentialRepresentationList.add(credential);
        userRepresentation.setCredentials(credentialRepresentationList);
        return userRepresentation;
    }
}
