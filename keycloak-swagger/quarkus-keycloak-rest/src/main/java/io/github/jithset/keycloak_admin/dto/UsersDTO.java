package io.github.jithset.keycloak_admin.dto;

import lombok.Getter;
import lombok.Setter;

public class UsersDTO {

    @Getter @Setter
    private String username;

    @Getter @Setter
    private String password;

    @Getter @Setter
    private String email;

    @Getter @Setter
    private boolean enabled = true;

}
