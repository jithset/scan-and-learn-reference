package io.github.jithset.utils;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
@Slf4j
public class ErrorMapper implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception exception) {
        log.error("Failed to handle request", exception);
        int code = 500;
        if (exception instanceof WebApplicationException) {
            code = ((WebApplicationException) exception).getResponse().getStatus();
        }
        Map<String, Object> result = new HashMap<>();
        result.put("status", code);
        result.put("message", exception.getMessage());
        return Response.status(code)
                .entity(result)
                .build();
    }
}
