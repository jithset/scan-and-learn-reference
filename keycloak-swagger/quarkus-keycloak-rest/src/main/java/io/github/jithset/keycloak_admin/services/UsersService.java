package io.github.jithset.keycloak_admin.services;

import io.github.jithset.keycloak_admin.config.KeycloakConfigurations;
import io.github.jithset.keycloak_admin.dto.UsersCreateDTO;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.UserRepresentation;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
public class UsersService {

    @Inject
    KeycloakConfigurations configurations;

    public UsersCreateDTO create(Keycloak keycloak, UserRepresentation userRepresentation) {
        Response result = keycloak.realm(configurations.realm()).users().create(userRepresentation);

        if (result.getStatus() != Response.Status.CREATED.getStatusCode()) {
            if (result.getStatus() == Response.Status.CONFLICT.getStatusCode()) {
                throw new WebApplicationException("Username already exists.", Response.Status.BAD_REQUEST);
            } else {
                throw new WebApplicationException("Unable to create user. Keycloak responded with status " + result.getStatus(),
                        Response.Status.BAD_REQUEST);
            }
        }
        String userId = getCreatedUserId(result);
        UsersCreateDTO usersCreateDTO = new UsersCreateDTO();
        usersCreateDTO.setUserId(userId);
        return usersCreateDTO;
    }

    // Extract UserId
    private String getCreatedUserId(Response response) {
        MultivaluedMap<String, Object> httpHeaders = response.getHeaders();
        List<Object> list = httpHeaders.get("Location");
        if (list != null && list.size() > 0) {
            return list.get(0).toString().replaceAll(".*/(.*)$", "$1");
        } else {
            return "";
        }
    }
}
