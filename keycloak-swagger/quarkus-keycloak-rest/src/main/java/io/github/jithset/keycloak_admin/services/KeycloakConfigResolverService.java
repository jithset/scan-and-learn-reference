package io.github.jithset.keycloak_admin.services;

import io.github.jithset.keycloak_admin.config.KeycloakConfigurations;
import org.keycloak.admin.client.Keycloak;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class KeycloakConfigResolverService {

    @Inject
    KeycloakConfigurations configurations;

    public Keycloak getKeycloak() {
        String SERVER_URL =  configurations.serverUrl();
        String REALM  = configurations.realm();
        String USERNAME  = configurations.username();
        String PASSWORD  = configurations.password();
        String CLIENT_ID  = configurations.clientId();
        String CLIENT_SECRET = configurations.clientSecret();

        return Keycloak.getInstance(SERVER_URL, REALM, USERNAME, PASSWORD, CLIENT_ID, CLIENT_SECRET);
    }

    public void serverInfo() {
        Keycloak instance =  getKeycloak();
        System.out.println("System Version" + instance.serverInfo().getInfo().getSystemInfo().getVersion());
    }
}
