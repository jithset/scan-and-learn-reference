package io.github.jithset;

import io.quarkus.security.Authenticated;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/admin")
@SecurityRequirement(name = "oauth2" )
@Produces(MediaType.TEXT_PLAIN)
public class AdminResource {

    @GET
    public String get() {
        return "Admin Get";
    }
}
