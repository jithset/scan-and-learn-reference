package io.github.jithset.keycloak_admin.config;


import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "keycloak")
public interface KeycloakConfigurations {
    String serverUrl();
    String username();
    String password();
    String clientId();
    String clientSecret();
    String realm();
}
