package io.github.jithset;

import org.eclipse.microprofile.openapi.annotations.Components;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlow;
import org.eclipse.microprofile.openapi.annotations.security.OAuthFlows;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        info = @Info(
                title="Example API",
                version = "1.0.0",
                contact = @Contact(
                        name = "Example API Support",
                        url = "http://exampleurl.com/contact",
                        email = "techsupport@example.com"),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html")),

//                security = @SecurityRequirement(name = "oauth2"),
                components = @Components(
                        securitySchemes = {
                                @SecurityScheme(
                                        securitySchemeName = "oauth2",
                                        type = SecuritySchemeType.OAUTH2,
                                        in = SecuritySchemeIn.HEADER,
                                        flows =  @OAuthFlows(
                                                password = @OAuthFlow(
                                                        tokenUrl = "http://localhost:8080/auth/realms/quarkus-rest/protocol/openid-connect/token"
                                                )
                                        )
                                )
                        })
)
public class QuarkusApplication extends Application {


}
