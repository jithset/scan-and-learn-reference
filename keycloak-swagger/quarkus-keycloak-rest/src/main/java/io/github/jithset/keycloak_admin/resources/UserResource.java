package io.github.jithset.keycloak_admin.resources;

import io.github.jithset.keycloak_admin.dto.UsersCreateDTO;
import io.github.jithset.keycloak_admin.dto.UsersDTO;
import io.github.jithset.keycloak_admin.mappers.UserMapper;
import io.github.jithset.keycloak_admin.services.KeycloakConfigResolverService;
import io.github.jithset.keycloak_admin.services.UsersService;
import org.keycloak.representations.idm.UserRepresentation;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    UsersService userService;


    @Inject
    KeycloakConfigResolverService keycloakConfigResolverService;

    @POST
    public UsersCreateDTO create(UsersDTO usersDTO) {
        UserRepresentation userRepresentation = UserMapper.fromDtoToRepresentation(usersDTO);
        return userService.create(keycloakConfigResolverService.getKeycloak(), userRepresentation);
    }
}
